<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class CycleOffersException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->message = "Cycle Problem";
        $this->code = Response::HTTP_BAD_REQUEST;
    }
}
