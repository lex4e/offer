<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class DuplicateUniqException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->message = "Value should be uniq";
        $this->code = Response::HTTP_BAD_REQUEST;
    }
}
