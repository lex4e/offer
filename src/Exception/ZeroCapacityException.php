<?php

namespace App\Exception;


use Symfony\Component\HttpFoundation\Response;

class ZeroCapacityException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->message = "There is not Offer with 0 capacity";
        $this->code = Response::HTTP_BAD_REQUEST;
    }
}
