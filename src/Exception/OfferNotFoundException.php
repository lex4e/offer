<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class OfferNotFoundException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->message = "Offer not exists";
        $this->code = Response::HTTP_NOT_FOUND;
    }
}
