<?php

namespace App\Repository;

use App\Entity\Offer;
use App\Entity\Partner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Partner>
 *
 * @method Partner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Partner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Partner[]    findAll()
 * @method Partner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartnerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Partner::class);
    }

    public function add(Partner $entity, bool $flush = true): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function countOffersPerLastDay($entityId, $period): int
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->andWhere('u.offer = :id')
            ->andWhere('u.created > :start')
            ->andWhere('u.created <= :end')
            ->setParameter('id', $entityId)
            ->setParameter('start', new \DateTime($period))
            ->setParameter('end', new \DateTime('now'))
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @param string $email
     * @param Offer $offer
     * @return Partner
     * @throws \Exception
     */
    public function create(string $email, Offer $offer)
    {
        $partner = new Partner();
        $partner->setOffer($offer);
        $partner->setEmail($email);
        $partner->setCreated(new \DateTime('now'));
        $this->add($partner);

        return $partner;
    }
}
