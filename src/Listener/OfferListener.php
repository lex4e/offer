<?php

namespace App\Listener;

use App\Entity\Offer;
use App\Exception\OfferNotFoundException;
use App\Repository\OfferRepository;
use App\Service\OfferService;
use App\Validator\Validator;
use Doctrine\ORM\Event\LifecycleEventArgs;

class OfferListener
{
    private $offerService;
    private $repository;
    private $validator;

    public function __construct(OfferService $offerService, OfferRepository $repository, Validator $validator)
    {
        $this->offerService = $offerService;
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * @param LifecycleEventArgs $args
     * @return null
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $offer = $args->getEntity();

        if (!$offer instanceof Offer) return null;

        if ($offer->getBackup() instanceof Offer) {
            $backup = $this->repository->find((int)$offer->getBackup()->getId());
            if (!$backup instanceof Offer) {
                throw new OfferNotFoundException();
            }
            $offer->setBackup($backup);
        }

        $this->validator->validate($offer);
        $this->offerService->checkOnEmptyBackupWhereCapacityMoreZero($offer);
    }

    /**
     * @param LifecycleEventArgs $args
     * @return null
     * @throws \Exception
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $offer = $args->getEntity();

        if (!$offer instanceof Offer) return null;

        $this->validator->validate($offer);
        $this->offerService->checkOnEmptyBackupWhereCapacityMoreZero($offer);
        $this->offerService->checkOnCycleProblem($offer);
    }
}
