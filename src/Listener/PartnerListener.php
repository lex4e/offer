<?php

namespace App\Listener;

use App\Entity\Partner;
use App\Service\OfferService;
use App\Validator\Validator;
use Doctrine\ORM\Event\LifecycleEventArgs;

class PartnerListener
{
    private $offerService;
    private $validator;

    public function __construct(OfferService $offerService, Validator $validator)
    {
        $this->offerService = $offerService;
        $this->validator = $validator;
    }

    /**
     * @param LifecycleEventArgs $args
     * @return null
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $partner = $args->getEntity();

        if (!$partner instanceof Partner) return null;

        $partner->setCreated(new \DateTime('now'));
        $this->validator->validate($partner);
    }
}
