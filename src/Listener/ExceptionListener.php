<?php

namespace App\Listener;

use App\Exception\DuplicateUniqException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    /**
     * @param ExceptionEvent $event
     * @throws DuplicateUniqException
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        if ($exception instanceof UniqueConstraintViolationException) {
            throw new DuplicateUniqException();
        } else {
            $message = json_decode($exception->getMessage(), true) ?? null;
            $response = new JsonResponse(["errors" => is_array($message) ? $message : [$exception->getMessage()]], 400);
        }
        $event->setResponse($response);
    }
}
