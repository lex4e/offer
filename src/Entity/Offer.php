<?php

namespace App\Entity;

use App\Repository\OfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OfferRepository::class)
 */
class Offer implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message = "offer: Empty field {{ value }}")
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "offer: Incorrect length of Offer",
     *      maxMessage = "offer: Length of Offer cannot be longer than {{ limit }} characters"
     * )
     */
    private $offer;

    /**
     * @ORM\ManyToOne(targetEntity=Offer::class)
     */
    private $backup;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     *
     * @Assert\NotBlank(message = "capacity: Empty field")
     * @Assert\PositiveOrZero(message = "capacity: Capacity of Offer should be positive number or zero")
     */
    private $capacity = 0;

    /**
     * Offer constructor.
     * @param int|null $id
     * @param string|null $offer
     * @param int|null $capacity
     * @param Offer|null $entity
     */
    public function __construct($id = null, $offer = null, $capacity = null, $entity = null)
    {
        $this->id = $id;
        $this->offer = $offer;
        $this->capacity = $capacity;
        $this->backup = $entity;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Offer|null
     */
    public function getBackup(): ?self
    {
        return $this->backup;
    }

    /**
     * @param self|null $backup
     */
    public function setBackup(?Offer $backup): void
    {
        $this->backup = $backup;
    }

    /**
     * @return String|null
     */
    public function getOffer(): ?string
    {
        return $this->offer;
    }

    /**
     * @param String|null $offer
     */
    public function setOffer(?string $offer): void
    {
        $this->offer = $offer;
    }

    /**
     * @return integer|null
     */
    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    /**
     * @param integer|null $capacity
     */
    public function setCapacity(?int $capacity): void
    {
        $this->capacity = $capacity;
    }
}
