<?php

namespace App\Validator;

use App\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param EntityInterface $entity
     * @throws \Exception
     */
    public function validate(EntityInterface $entity)
    {
        $errors = $this->validator->validate($entity);
        if (count($errors) > 0) {
            $messages = [];
            foreach ($errors as $error) {
                $messages[] = $error->getMessage();
            }
            throw new \Exception(json_encode($messages), Response::HTTP_BAD_REQUEST);
        }
    }
}
