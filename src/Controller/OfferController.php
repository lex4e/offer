<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Exception\OfferNotFoundException;
use App\Repository\OfferRepository;
use App\Service\OfferDTO;
use App\Service\OfferService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OfferController extends AbstractController
{
    /**
     * @param OfferRepository $offerRepository
     * @return JsonResponse
     *
     * @Route("/offer", name="app_offer", methods={"GET", "OPTIONS"})
     */
    public function index(OfferRepository $offerRepository): JsonResponse
    {
        return $this->json(OfferDTO::getOffersListByOffersAsArray($offerRepository->findAll()));
    }

    /**
     * @param int $id
     * @param OfferRepository $offerRepository
     * @return JsonResponse
     * @throws OfferNotFoundException
     *
     * @Route("/offer/{id}", name="app_offer_details", methods={"GET", "OPTIONS"})
     */
    public function view(int $id, OfferRepository $offerRepository): JsonResponse
    {
        $offer = $offerRepository->find($id);
        if (!$offer instanceof Offer) {
            throw new OfferNotFoundException();
        }

        return $this->json(OfferDTO::getOfferByOfferAsArray($offerRepository->find($id)));
    }

    /**
     * @param int $id
     * @param OfferRepository $offerRepository
     * @param OfferService $service
     * @return JsonResponse
     * @throws OfferNotFoundException
     *
     * @Route("/offer/{id}/chain", name="app_offer_chain", methods={"GET", "OPTIONS"})
     */
    public function chain(int $id, OfferRepository $offerRepository, OfferService $service): JsonResponse
    {
        $offer = $offerRepository->find($id);
        if (!$offer instanceof Offer) {
            throw new OfferNotFoundException();
        }
        $service->checkOnCycleProblem($offer);

        return $this->json($service->getChain($offer));
    }

    /**
     * @param Request $request
     * @param OfferRepository $offerRepository
     * @return JsonResponse
     *
     * @Route("/offer", name="app_offer_create", methods={"POST", "OPTIONS"})
     */
    public function create(Request $request, OfferRepository $offerRepository): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $offerDTO = new OfferDTO($data);
        $offer = $offerDTO->getInstance();
        $offerRepository->save($offer);

        return $this->json(OfferDTO::getOfferByOfferAsArray($offer));
    }

    /**
     * @param int $id
     * @param Request $request
     * @param OfferRepository $offerRepository
     * @return JsonResponse
     * @throws OfferNotFoundException
     *
     * @Route("/offer/{id}", name="app_offer_update", methods={"PUT", "OPTIONS"})
     */
    public function update(int $id, Request $request, OfferRepository $offerRepository): JsonResponse
    {
        $offer = $offerRepository->find($id);
        if (!$offer instanceof Offer) {
            throw new OfferNotFoundException();
        }

        $data = json_decode($request->getContent(), true);
        empty($data['offer']) ? true : $offer->setOffer($data['offer']);
        empty($data['capacity']) ? true : $offer->setCapacity((int)$data['capacity']);

        if (!empty($data['backup'])) {
            $backup = $offerRepository->find((int)$data['backup']);
            if (!$backup instanceof Offer) {
                throw new OfferNotFoundException();
            }
            $offer->setBackup($backup);
        } else {
            $offer->setBackup(null);
        }

        $offerRepository->save($offer);

        return $this->json(OfferDTO::getOfferByOfferAsArray($offer));
    }
}
