<?php

namespace App\Controller;

use App\Exception\OfferNotFoundException;
use App\Repository\OfferRepository;
use App\Repository\PartnerRepository;
use App\Service\OfferService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PartnerController extends AbstractController
{
    /**
     * @param Request $request
     * @param PartnerRepository $partnerRepository
     * @param OfferRepository $offerRepository
     * @return JsonResponse
     * @throws OfferNotFoundException
     *
     * @Route("/partner", name="app_partner", methods={"POST"})
     */
    public function index(Request $request, PartnerRepository $partnerRepository,
                          OfferRepository $offerRepository, OfferService $offerService): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data['offer'])) {
            throw new OfferNotFoundException();
        }
        $offer = $offerRepository->find((int)$data['offer']);
        if (empty($offer)) {
            throw new OfferNotFoundException();
        }

        $partner = $partnerRepository->create($data['email'], $offerService->getOffer($offer));

        return $this->json($partner);
    }
}
