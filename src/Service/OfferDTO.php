<?php

namespace App\Service;

use App\Entity\Offer;

class OfferDTO
{
    /** @var int */
    private $id;

    /** @var string */
    private $offer;

    private $backup;

    /** @var int */
    private $capacity;

    public function __construct(array $data)
    {
        $this->setId($data['id'] ?? null);
        $this->setOffer($data['offer'] ?? null);
        $this->setCapacity($data['capacity'] ?? null);
        $this->setBackup($data['backup'] ?? null);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param string|null $offer
     */
    public function setOffer(?string $offer): void
    {
        $this->offer = $offer;
    }

    /**
     * @return int|null
     */
    public function getBackup(): ?int
    {
        return $this->backup;
    }

    public function setBackup($backup): void
    {
        $this->backup = $backup;
    }

    /**
     * @return int
     */
    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    /**
     * @param $capacity
     */
    public function setCapacity($capacity): void
    {
        $this->capacity = $capacity;
    }

    /**
     * @return Offer
     */
    public function getInstance()
    {
        return new Offer($this->id, $this->offer, $this->capacity,
            $this->getBackup() ? new Offer($this->getBackup()) : null);
    }

    public static function getOfferByOfferAsArray(Offer $offer)
    {
        return [
            'id' => $offer->getId(),
            'offer' => $offer->getOffer(),
            'capacity' => $offer->getCapacity(),
            'backup_id' => $offer->getBackup() instanceof Offer ? $offer->getBackup()->getId() : null
        ];
    }

    public static function getOffersListByOfferAsArray(Offer $offer)
    {
        $list = [self::getOfferByOfferAsArray($offer)];
        while (($offer = $offer->getBackup()) instanceof Offer) $list[] = self::getOfferByOfferAsArray($offer);

        return $list;
    }

    public static function getOffersListByOffersAsArray(array $offers)
    {
        $list = [];
        foreach ($offers as $offer) {
            $list[] = self::getOfferByOfferAsArray($offer);
        }
        return $list;
    }
}
