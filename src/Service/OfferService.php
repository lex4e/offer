<?php

namespace App\Service;

use App\Entity\Offer;
use App\Exception\CycleOffersException;
use App\Exception\OfferNotFoundException;
use App\Exception\ZeroCapacityException;
use App\Repository\OfferRepository;
use App\Repository\PartnerRepository;

class OfferService
{
    private $repository;
    private $partnerRepository;

    public function __construct(OfferRepository $repository, PartnerRepository $partnerRepository)
    {
        $this->repository = $repository;
        $this->partnerRepository = $partnerRepository;
    }

    /**
     * @param Offer $offer
     * @return Offer
     * @throws OfferNotFoundException
     */
    public function getOffer(Offer $offer)
    {
        $period = getenv("OFFERS_PERIOD");
        $offers = $this->repository->allAsArray();
        if (empty($offers)) throw new OfferNotFoundException();
        $offer = OfferDTO::getOfferByOfferAsArray($offer);

        while ($offer['capacity'] > 0
            && $offer['capacity'] <= $this->partnerRepository->countOffersPerLastDay($offer['id'], $period)) {

            $offer = $offers[$offer['backup_id']];
            if (empty($offer)) throw new OfferNotFoundException();
        }
        return (new OfferDTO($offer))->getInstance();
    }

    /**
     * @param Offer $offer
     * @throws \Exception
     */
    public function checkOnCycleProblem(Offer $offer)
    {
        if ($offer->getBackup() instanceof Offer) {
            $offers = $this->repository->allAsArray();
            if ($offer->getId() === $offer->getBackup()->getId()) throw new CycleOffersException();
            $ids = [$offer->getId()];
            $curOffer = $offers[$offer->getBackup()->getId()];
            while (!empty($curOffer)) {
                if (in_array($curOffer['id'], $ids)) {
                    throw new CycleOffersException();
                }
                $ids[] = $curOffer['id'];
                if (empty($curOffer['backup_id'])) break;
                $curOffer = $offers[$curOffer['backup_id']];
            }
        }
    }

    /**
     * @param Offer $offer
     * @return array
     */
    public function getChain(Offer $offer)
    {
        if (!$offer->getBackup() instanceof Offer) return [$offer];

        $offers = $this->repository->allAsArray();
        $chain = [$offers[$offer->getId()]];
        $offer = $offers[$offer->getBackup()->getId()];
        while (!empty($offer)) {
            $chain[] = $offer;
            if (empty($offer['backup_id'])) break;
            $offer = $offers[$offer['backup_id']];
        }

        return $chain;
    }

    /**
     * @param Offer $offer
     * @throws \Exception
     */
    public function checkOnEmptyBackupWhereCapacityMoreZero(Offer $offer)
    {
        if ($offer->getCapacity() === 0) return;
        if (!$offer->getBackup() instanceof Offer) {
            throw new ZeroCapacityException();
        }
        $offers = $this->repository->allAsArray();
        $offer = $offers[$offer->getBackup()->getId()];
        while (!empty($offers)) {
            if ($offer['capacity'] === 0) return;
            if (empty($offer['backup_id'])) break;
            $offer = $offers[$offer['backup_id']];
        }
        throw new ZeroCapacityException();
    }
}
