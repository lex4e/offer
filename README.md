Installation.

1.  Copy .env.example to .env and .env.test, 
    change db_user, db_pass, db_name in these files 
2. Run command: php bin/console doctrine:database:create 
3. Run command: php bin/console doctrine:migrations:migrate
4. Run command: cd public
5. Run command: php -S 127.0.0.1:2000

Endpoints.

Headers: [Content-type: application/json]


1. Create Offer (without backup)

POST http://127.0.0.1:2000/offer

Request body:
{
    "offer": "some offer content",
    "capacity": 0,
    "backup": null
}

2. Create Offer (with backup)
   
POST http://127.0.0.1:2000/offer

Request body:
{
   "offer": "some offer content",
   "capacity": 10,
   "backup_id": 1 
}

3. Update Offer
   
PUT http://127.0.0.1:2000/offer/3

Request body:
{
    "offer": "some offer content",
    "capacity": 10,
    "backup": 2
}


4. Read Offer (with all backups)
   
GET http://127.0.0.1:2000/offer/3

Response body:
{
    "id": 1,
    "backup_id": 3,
    "offer": "some offer",
    "capacity": 3
}

5. Chain by Offer 

GET http://127.0.0.1:2000/offer/4/chain

Response body:

[
    {
        "id": 4,
        "offer": "Ofer3",
        "capacity": 1,
        "backup_id": 3
    },
    {
        "id": 3,
        "offer": "Ofer3",
        "capacity": 3,
        "backup_id": 2
    },
    ...,
    {
        "id": 1,
        "offer": "Ofer1",
        "capacity": 0,
        "backup_id": null
    }
]

6. Read Offer (without all backups)
   
GET http://127.0.0.1:2000/offer/3

Response body:
{
    "id": 4,
    "offer": "Offer3",
    "capacity": 1,
    "backup_id": 3
}


7. Read Offers
   
GET http://127.0.0.1:2000/offer

Response body:
[
    {
        "id": 1,
        "offer": "Ofer1",
        "capacity": 0,
        "backup_id": null
    },
    {
        "id": 2,
        "offer": "Ofer2222",
        "capacity": 1,
        "backup_id": 1
    },
    ...
]    


8. Create Offer for Partner

POST http://127.0.0.1:2000/partner

Request body:
{
    "offer": 3,
	"email": "partner@email.test" 
}

Response:
{
    "id": 5,
    "email": "partner@email.test",
    "offer": 3,
    "created": "2022-08-03T16:36:53+03:00"
}
